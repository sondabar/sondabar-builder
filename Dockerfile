FROM adoptopenjdk/openjdk8

LABEL description="Builder and deployer image for the www.sondabar.de website"
LABEL maintainer="lars@cormann.biz"

ENV HUGO_VERSION 0.55.6
ENV HUGO_DEB hugo_${HUGO_VERSION}_Linux-64bit.deb
ENV HUGO_URL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_DEB}

ENV DOCTL_VERSION 1.16.0
ENV DOCTL_URL https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz

RUN apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install jq ruby wget apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y \
 && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
 && add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable" \
 && curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
 && touch /etc/apt/sources.list.d/kubernetes.list \
 && echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list \
 && apt-get update \
 && apt-get install kubectl docker-ce docker-ce-cli containerd.io -y \
 && gem install s3_website \
 && curl -sL ${DOCTL_URL} | tar -xzv \
 && mv doctl /usr/local/bin \
 && rm -f ~/doctl \
 && curl -sLO ${HUGO_URL} \
 && dpkg -i ${HUGO_DEB} \
 && rm -f ${HUGO_DEB} \
 && rm -rf /tmp/* /var/tmp/* \
 && apt-get -y clean \
 && rm -rf /var/lib/apt/lists/*